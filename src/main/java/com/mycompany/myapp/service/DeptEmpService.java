package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.DeptEmpDTO;

public interface DeptEmpService {
	
	List<DeptEmpDTO> findDeptEmpByEmpNo(int empNo);

}
