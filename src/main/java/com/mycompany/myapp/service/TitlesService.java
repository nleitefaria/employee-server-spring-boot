package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.TitlesDTO;

public interface TitlesService
{
	public List<TitlesDTO> findTitlesByEmpNo(int empNo);
	List<TitlesDTO> findAll();
	PagginatedDTO<TitlesDTO> findAllPaginated(int pageNum, int pageSize);
	void save(TitlesDTO titlesDTO) throws javax.persistence.RollbackException;
}
