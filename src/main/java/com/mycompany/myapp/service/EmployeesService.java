package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface EmployeesService 
{
	EmployeesDTO findOne(int id);
	List<EmployeesDTO> findAll();
	PagginatedDTO<EmployeesDTO> findAllPaginated(int pageNum, int pageSize);
	void save(EmployeesDTO employeesDTO) throws javax.persistence.RollbackException;
	EmployeesDTO update(int id, EmployeesDTO employeesDTO) throws javax.persistence.RollbackException;
	int delete(int id) throws javax.persistence.RollbackException;
}
