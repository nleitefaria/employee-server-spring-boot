package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.DeptManagerDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Departments;
import com.mycompany.myapp.entity.DeptManager;
import com.mycompany.myapp.entity.DeptManagerId;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.repository.DepartmentsRepository;
import com.mycompany.myapp.repository.DeptManagerRepository;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.service.DeptManagerService;

@Service
public class DeptManagerServiceImpl implements DeptManagerService
{
	@Autowired
	DeptManagerRepository deptManagerRepository;
	
	@Autowired
	DepartmentsRepository departmentsRepository;
	
	@Autowired
	EmployeesRepository employeesRepository;
	
	@Transactional
	public List<DeptManagerDTO> findDeptManagerByEmpNo(int empNo)
	{
		List<DeptManagerDTO> deptManagerDTOList = new ArrayList<DeptManagerDTO>();
		List<DeptManager> deptManagerList = deptManagerRepository.findDeptManagersByEmpNo(empNo);
		
		for(DeptManager deptManager : deptManagerList)
		{
			DeptManagerDTO deptManagerDTO = new DeptManagerDTO(deptManager);		
			deptManagerDTOList.add(deptManagerDTO);
		}
		
		return deptManagerDTOList;	
	}
	
	@Transactional
	public List<DeptManagerDTO> findAll()
	{
		List<DeptManagerDTO> deptManagerDTOList = new ArrayList<DeptManagerDTO>();
		List<DeptManager> deptManagerList = deptManagerRepository.findAll();
		
		for(DeptManager deptManager : deptManagerList)
		{
			DeptManagerDTO deptManagerDTO = new DeptManagerDTO(deptManager);
			deptManagerDTOList.add(deptManagerDTO);
		}
		
		return deptManagerDTOList;
	}
	
	@Transactional
	public PagginatedDTO<DeptManagerDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<DeptManagerDTO> deptManagerDTOList = new ArrayList<DeptManagerDTO>();				
		Page<DeptManager> deptManagerPage = deptManagerRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(DeptManager deptManager : deptManagerPage.getContent())
		{
			DeptManagerDTO deptManagerDTO = new DeptManagerDTO(deptManager);
			deptManagerDTOList.add(deptManagerDTO);		
		}
		
		PagginatedDTO<DeptManagerDTO> departmentsDTOPage = new PagginatedDTO<DeptManagerDTO>(deptManagerDTOList, deptManagerPage.getNumber(), deptManagerPage.getNumberOfElements(), deptManagerPage.getTotalPages(), deptManagerPage.getTotalElements());
		return departmentsDTOPage;
	}	
	
	@Transactional
	public void save(DeptManagerDTO deptManagerDTO) throws javax.persistence.RollbackException   
	{		
		DeptManagerId id = new DeptManagerId(deptManagerDTO.getEmployees().getEmpNo(), deptManagerDTO.getDepartments().getDeptNo());
		
		Departments departments = departmentsRepository.findOne(deptManagerDTO.getDepartments().getDeptNo());		
		if(departments == null)
		{
			departments = new Departments(deptManagerDTO.getDepartments().getDeptNo(), deptManagerDTO.getDepartments().getDeptName());
			departmentsRepository.save(departments);
		}
		
		Employees employees = employeesRepository.findOne(deptManagerDTO.getEmployees().getEmpNo());
		if(employees == null)
		{
			employees = new Employees(deptManagerDTO.getEmployees().getEmpNo(), deptManagerDTO.getEmployees().getBirthDate(), deptManagerDTO.getEmployees().getFirstName(), deptManagerDTO.getEmployees().getLastName(), deptManagerDTO.getEmployees().getGender(), deptManagerDTO.getEmployees().getHireDate());
			employeesRepository.save(employees);
		}
		
		DeptManager deptManager = new DeptManager(id, departments, employees, deptManagerDTO.getFromDate(), deptManagerDTO.getToDate());	
		deptManagerRepository.save(deptManager);	
	}
}
