package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Departments;
import com.mycompany.myapp.repository.DepartmentsRepository;
import com.mycompany.myapp.service.DepartmentsService;

@Service
public class DepartmentsServiceImpl implements DepartmentsService 
{
	@Autowired
	DepartmentsRepository departmentsRepository;
	
	@Transactional
	public DepartmentsDTO findOne(String id)
	{
		Departments departments = departmentsRepository.findOne(id);
		return new DepartmentsDTO(departments);
	}
	
	@Transactional
	public List<DepartmentsDTO> findAll()
	{
		List<DepartmentsDTO> departmentsDTOList = new ArrayList<DepartmentsDTO>();
		List<Departments> departmentsList = departmentsRepository.findAll();
		
		for(Departments departments : departmentsList)
		{
			DepartmentsDTO departmentsDTO = new DepartmentsDTO(departments);
			departmentsDTOList.add(departmentsDTO);
		}
		
		return departmentsDTOList;
	}
	
	@Transactional
	public PagginatedDTO<DepartmentsDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<DepartmentsDTO> departmentsDTOList = new ArrayList<DepartmentsDTO>();				
		Page<Departments> departmentsPage = departmentsRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Departments departments : departmentsPage.getContent())
		{
			DepartmentsDTO departmentsDTO = new DepartmentsDTO(departments);
			departmentsDTOList.add(departmentsDTO);		
		}
		
		PagginatedDTO<DepartmentsDTO> departmentsDTOPage = new PagginatedDTO<DepartmentsDTO>(departmentsDTOList, departmentsPage.getNumber(), departmentsPage.getNumberOfElements(), departmentsPage.getTotalPages(), departmentsPage.getTotalElements());
		return departmentsDTOPage;
	}
	
	@Transactional
	public void save(DepartmentsDTO departmentsDTO) throws javax.persistence.RollbackException   
	{		
		Departments departments = new Departments(departmentsDTO.getDeptNo(), departmentsDTO.getDeptName());		
		departmentsRepository.save(departments);	
	}
	
	@Transactional
	public DepartmentsDTO update(String id, DepartmentsDTO departmentsDTO) throws javax.persistence.RollbackException   
	{		
		Departments department = departmentsRepository.findOne(id);
		
		if(department != null)
		{
			department.setDeptName(departmentsDTO.getDeptName());
			DepartmentsDTO ret = new DepartmentsDTO(department);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public String delete(String id) throws javax.persistence.RollbackException   
	{		
		Departments department = departmentsRepository.findOne(id);
		
		if(department != null)
		{
			departmentsRepository.delete(department);
			return id;
		}
		else
		{
			return null;		
		}			
	}
}
