package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.myapp.domain.DeptEmpDTO;
import com.mycompany.myapp.entity.DeptEmp;
import com.mycompany.myapp.repository.DeptEmpRepository;
import com.mycompany.myapp.service.DeptEmpService;

@Service
public class DeptEmpServiceImpl implements DeptEmpService 
{	
	@Autowired
	DeptEmpRepository deptEmpRepository;
	
	public List<DeptEmpDTO> findDeptEmpByEmpNo(int empNo)
	{
		List<DeptEmpDTO> deptEmpDTOList = new ArrayList<DeptEmpDTO>();
		List<DeptEmp> deptEmpList = deptEmpRepository.findDeptEmpByEmpNo(empNo);
		
		for(DeptEmp deptEmp : deptEmpList)
		{
			DeptEmpDTO deptEmpDTO = new DeptEmpDTO(deptEmp);
			deptEmpDTOList.add(deptEmpDTO);		
		}
		
		return deptEmpDTOList;	
	}

}
