package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.TitlesDTO;
import com.mycompany.myapp.domain.TitlesIdDTO;

import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.entity.Titles;
import com.mycompany.myapp.entity.TitlesId;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.repository.TitlesRepository;
import com.mycompany.myapp.service.TitlesService;

@Service
public class TitlesServiceImpl implements TitlesService
{
	@Autowired
	TitlesRepository titlesRepository;
	
	@Autowired
	EmployeesRepository employeesRepository;
		
	@Transactional
	public List<TitlesDTO> findTitlesByEmpNo(int empNo)
	{
		List<TitlesDTO> titlesDTOList = new ArrayList<>();
		List<Titles> titlesList = titlesRepository.findTitlesByEmpNo(empNo);	
		
		for(Titles titles: titlesList)
		{			
			TitlesIdDTO titlesIdDTO = new TitlesIdDTO(titles.getId());
			EmployeesDTO employeesDTO = new EmployeesDTO(titles.getEmployees());
			TitlesDTO titlesDTO = new TitlesDTO(titlesIdDTO, employeesDTO, titles.getToDate());		
			titlesDTOList.add(titlesDTO);
		}		
		
		return titlesDTOList;
	}

	@Override
	public List<TitlesDTO> findAll() 
	{
		List<TitlesDTO> titlesDTOList = new ArrayList<>();
		List<Titles> titlesList = titlesRepository.findAll();
		
		for(Titles titles: titlesList)
		{			
			TitlesIdDTO titlesIdDTO = new TitlesIdDTO(titles.getId());
			EmployeesDTO employeesDTO = new EmployeesDTO(titles.getEmployees());
			TitlesDTO titlesDTO = new TitlesDTO(titlesIdDTO, employeesDTO, titles.getToDate());		
			titlesDTOList.add(titlesDTO);
		}		
		
		return titlesDTOList;
	}
	
	@Transactional
	public PagginatedDTO<TitlesDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<TitlesDTO> titlesDTOList = new ArrayList<TitlesDTO>();				
		Page<Titles> titlesPage = titlesRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Titles titles : titlesPage.getContent())
		{
			TitlesIdDTO titlesIdDTO = new TitlesIdDTO(titles.getId());
			EmployeesDTO employeesDTO = new EmployeesDTO(titles.getEmployees());
			TitlesDTO titlesDTO = new TitlesDTO(titlesIdDTO, employeesDTO, titles.getToDate());		
			titlesDTOList.add(titlesDTO);		
		}
		
		PagginatedDTO<TitlesDTO> titlesDTOPage = new PagginatedDTO<TitlesDTO>(titlesDTOList, titlesPage.getNumber(), titlesPage.getNumberOfElements(), titlesPage.getTotalPages(), titlesPage.getTotalElements());
		return titlesDTOPage;
	}
	
	@Transactional
	public void save(TitlesDTO titlesDTO) throws javax.persistence.RollbackException   
	{		
		Employees employees = employeesRepository.findOne(titlesDTO.getEmployees().getEmpNo());
		TitlesId titlesId = new TitlesId(titlesDTO.getId().getEmpNo(), titlesDTO.getId().getTitle(), titlesDTO.getId().getFromDate());
		Titles titles = new Titles(titlesId, employees);		
		titlesRepository.save(titles);	
	}
	
}
