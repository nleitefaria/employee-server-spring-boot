package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.SalariesDTO;
import com.mycompany.myapp.domain.SalariesIdDTO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.entity.Salaries;
import com.mycompany.myapp.entity.SalariesId;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.repository.SalariesRepository;
import com.mycompany.myapp.service.SalariesService;

@Service
public class SalariesServiceImpl implements SalariesService {
	
	@Autowired
	SalariesRepository salariesRepository;
	
	@Autowired
	EmployeesRepository employeesRepository;
	
	@Transactional
	public List<SalariesDTO> findDeptManagerByEmpNo(int empNo)
	{
		List<SalariesDTO> salariesDTOList = new ArrayList<SalariesDTO>();
		List<Salaries> salariesList = salariesRepository.findSalariesByEmpNo(empNo);
		
		for(Salaries salaries : salariesList)
		{
			SalariesIdDTO id = new SalariesIdDTO(salaries.getId());
			EmployeesDTO employees = new EmployeesDTO(salaries.getEmployees());			
			SalariesDTO employeesDTO = new SalariesDTO(id, employees, salaries.getSalary(), salaries.getToDate());
			salariesDTOList.add(employeesDTO);
		}
		
		return salariesDTOList;	
	}
	
	@Transactional
	public List<SalariesDTO> findAll()
	{
		List<SalariesDTO> salariesDTOList = new ArrayList<SalariesDTO>();
		List<Salaries> salariesList = salariesRepository.findAll();	
		
		for(Salaries salaries : salariesList)
		{
			SalariesIdDTO id = new SalariesIdDTO(salaries.getId());
			EmployeesDTO employees = new EmployeesDTO(salaries.getEmployees());			
			SalariesDTO employeesDTO = new SalariesDTO(id, employees, salaries.getSalary(), salaries.getToDate());
			salariesDTOList.add(employeesDTO);		
		}
		
		return salariesDTOList;
	}
	
	@Transactional
	public PagginatedDTO<SalariesDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<SalariesDTO> salariesDTOList = new ArrayList<SalariesDTO>();				
		Page<Salaries> salariesPage = salariesRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Salaries salaries : salariesPage.getContent())
		{
			SalariesIdDTO id = new SalariesIdDTO(salaries.getId());
			EmployeesDTO employees = new EmployeesDTO(salaries.getEmployees());			
			SalariesDTO employeesDTO = new SalariesDTO(id, employees, salaries.getSalary(), salaries.getToDate());
			salariesDTOList.add(employeesDTO);		
		}
		
		PagginatedDTO<SalariesDTO> salariesDTOPage = new PagginatedDTO<SalariesDTO>(salariesDTOList, salariesPage.getNumber(), salariesPage.getNumberOfElements(), salariesPage.getTotalPages(), salariesPage.getTotalElements());
		return salariesDTOPage;
	}
	
	@Transactional
	public void save(SalariesDTO salariesDTO) throws javax.persistence.RollbackException   
	{	
		Employees employees = employeesRepository.findOne(salariesDTO.getEmployees().getEmpNo());	
		
		if(employees == null)
		{
			employees = new Employees(salariesDTO.getEmployees().getEmpNo(), salariesDTO.getEmployees().getBirthDate(), salariesDTO.getEmployees().getFirstName(), salariesDTO.getEmployees().getLastName(), salariesDTO.getEmployees().getGender(), salariesDTO.getEmployees().getHireDate());
			employeesRepository.save(employees);
		}
		
		SalariesId salariesId = new SalariesId(salariesDTO.getId().getEmpNo()  , salariesDTO.getId().getFromDate());
		Salaries salaries = new Salaries(salariesId, employees, salariesDTO.getSalary(), salariesDTO.getToDate());
		salariesRepository.save(salaries);	
	}
}
