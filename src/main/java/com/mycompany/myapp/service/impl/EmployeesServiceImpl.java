package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.repository.EmployeesRepository;
import com.mycompany.myapp.service.EmployeesService;

@Service
public class EmployeesServiceImpl implements EmployeesService
{
	@Autowired
	EmployeesRepository employeesRepository;
	
	@Transactional
	public EmployeesDTO findOne(int id)
	{				
		Employees employees = employeesRepository.findOne(id);		
		return new EmployeesDTO(employees);		
	}
	
	@Transactional
	public List<EmployeesDTO> findAll()
	{
		List<EmployeesDTO> employeesDTOList = new ArrayList<EmployeesDTO>();
		List<Employees> employeesList = employeesRepository.findAll();	
		
		for(Employees employees : employeesList)
		{
			EmployeesDTO employeesDTO = new EmployeesDTO(employees);			
			employeesDTOList.add(employeesDTO);			
		}
		
		return employeesDTOList;
	}
	
	@Transactional
	public PagginatedDTO<EmployeesDTO> findAllPaginated(int pageNum, int pageSize)
	{
		List<EmployeesDTO> employeesDTOList = new ArrayList<EmployeesDTO>();				
		Page<Employees> employeesPage = employeesRepository.findAll(new PageRequest(pageNum - 1, pageSize));
		
		for(Employees employees : employeesPage.getContent())
		{
			EmployeesDTO employeesDTO = new EmployeesDTO(employees);
			employeesDTOList.add(employeesDTO);		
		}
		
		PagginatedDTO<EmployeesDTO> employeesDTOPage = new PagginatedDTO<EmployeesDTO>(employeesDTOList, employeesPage.getNumber(), employeesPage.getNumberOfElements(), employeesPage.getTotalPages(), employeesPage.getTotalElements());
		return employeesDTOPage;
	}
	
	@Transactional
	public void save(EmployeesDTO employeesDTO) throws javax.persistence.RollbackException   
	{		
		Employees employees = new Employees(employeesDTO.getEmpNo(), employeesDTO.getBirthDate(), employeesDTO.getFirstName(), employeesDTO.getLastName(), employeesDTO.getGender(), employeesDTO.getHireDate());		
		employeesRepository.save(employees);	
	}
	
	@Transactional
	public EmployeesDTO update(int id, EmployeesDTO employeesDTO) throws javax.persistence.RollbackException   
	{		
		Employees employees = employeesRepository.findOne(id);
		
		if(employees != null)
		{
			employees.setFirstName(employeesDTO.getFirstName());
			employees.setLastName(employeesDTO.getLastName());
			employees.setGender(employeesDTO.getGender());
			
			EmployeesDTO ret = new EmployeesDTO(employees);
			return ret;
		}
		else
		{
			return null;		
		}			
	}
	
	@Transactional
	public int delete(int id) throws javax.persistence.RollbackException   
	{		
		Employees employees = employeesRepository.findOne(id);
		
		if(employees != null)
		{
			employeesRepository.delete(employees);
			return id;
		}
		else
		{
			return 0;		
		}			
	}
}
