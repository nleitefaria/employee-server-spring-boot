package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface DepartmentsService
{
	DepartmentsDTO findOne(String id);
	List<DepartmentsDTO> findAll();
	PagginatedDTO<DepartmentsDTO> findAllPaginated(int pageNum, int pageSize);
	void save(DepartmentsDTO departmentsDTO) throws javax.persistence.RollbackException;
	DepartmentsDTO update(String id, DepartmentsDTO departmentsDTO) throws javax.persistence.RollbackException;
	String delete(String id) throws javax.persistence.RollbackException;
}
