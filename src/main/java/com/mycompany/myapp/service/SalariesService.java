package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.SalariesDTO;

public interface SalariesService
{
	List<SalariesDTO> findAll();
	List<SalariesDTO> findDeptManagerByEmpNo(int empNo);
	PagginatedDTO<SalariesDTO> findAllPaginated(int pageNum, int pageSize);
	void save(SalariesDTO salariesDTO) throws javax.persistence.RollbackException;
}
