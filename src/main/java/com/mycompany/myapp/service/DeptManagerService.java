package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.domain.DeptManagerDTO;
import com.mycompany.myapp.domain.PagginatedDTO;

public interface DeptManagerService 
{	
	List<DeptManagerDTO> findDeptManagerByEmpNo(int empNo);
	List<DeptManagerDTO> findAll();
	PagginatedDTO<DeptManagerDTO> findAllPaginated(int pageNum, int pageSize);
	void save(DeptManagerDTO deptManagerDTO) throws javax.persistence.RollbackException;
}
