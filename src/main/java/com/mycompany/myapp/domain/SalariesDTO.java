package com.mycompany.myapp.domain;

import java.util.Date;


public class SalariesDTO {
	
	private SalariesIdDTO id;
	private EmployeesDTO employees;
	private int salary;
	private Date toDate;
	
	public SalariesDTO() 
	{		
	}
	
	public SalariesDTO(SalariesIdDTO id, EmployeesDTO employees, int salary, Date toDate)
	{	
		this.id = id;
		this.employees = employees;
		this.salary = salary;
		this.toDate = toDate;
	}

	public SalariesIdDTO getId() {
		return id;
	}

	public void setId(SalariesIdDTO id) {
		this.id = id;
	}

	public EmployeesDTO getEmployees() {
		return employees;
	}

	public void setEmployees(EmployeesDTO employees) {
		this.employees = employees;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
