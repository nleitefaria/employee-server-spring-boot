package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.DeptManagerId;

public class DeptManagerIdDTO
{
	private int empNo;
    private String deptNo;
   
	public DeptManagerIdDTO() 
	{
	}

	public DeptManagerIdDTO(int empNo, String deptNo) 
	{
		this.empNo = empNo;
		this.deptNo = deptNo;
	}
	
	public DeptManagerIdDTO(DeptManagerId deptManagerId) 
	{
		this.empNo = deptManagerId.getEmpNo();
		this.deptNo = deptManagerId.getDeptNo();
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
}
