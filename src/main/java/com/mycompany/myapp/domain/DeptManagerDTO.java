package com.mycompany.myapp.domain;

import java.util.Date;
import com.mycompany.myapp.entity.DeptManager;

public class DeptManagerDTO 
{
	private DeptManagerIdDTO deptManagerId;
	private DepartmentsDTO departments;
	private EmployeesDTO employees;
	private Date fromDate;
	private Date toDate;
	
	public DeptManagerDTO()
	{	
	}
	
	public DeptManagerDTO(DeptManagerIdDTO deptManagerId, DepartmentsDTO departments, EmployeesDTO employees, Date fromDate, Date toDate) 
	{
		this.deptManagerId = deptManagerId;
		this.departments = departments;
		this.employees = employees;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public DeptManagerDTO(DeptManager deptManager)
	{		
		this.deptManagerId = new DeptManagerIdDTO(deptManager.getId());
		this.departments = new DepartmentsDTO(deptManager.getDepartments());
		this.employees =  new EmployeesDTO(deptManager.getEmployees());
		this.fromDate = deptManager.getFromDate();
		this.toDate = deptManager.getToDate();
	}

	public DeptManagerIdDTO getDeptManagerId() {
		return deptManagerId;
	}

	public void setDeptManagerId(DeptManagerIdDTO deptManagerId) {
		this.deptManagerId = deptManagerId;
	}

	public DepartmentsDTO getDepartments() {
		return departments;
	}

	public void setDepartments(DepartmentsDTO departments) {
		this.departments = departments;
	}

	public EmployeesDTO getEmployees() {
		return employees;
	}

	public void setEmployees(EmployeesDTO employees) {
		this.employees = employees;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
