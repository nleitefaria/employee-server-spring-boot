package com.mycompany.myapp.domain;

import java.util.Date;
import com.mycompany.myapp.entity.DeptEmp;

public class DeptEmpDTO 
{	
	private DeptEmpIdDTO id;
    private DepartmentsDTO departments;
    private EmployeesDTO employees;
    private Date fromDate;
    private Date toDate;
    
    public DeptEmpDTO() 
    {	
	}
    
	public DeptEmpDTO(DeptEmpIdDTO id, DepartmentsDTO departments, EmployeesDTO employees, Date fromDate, Date toDate)
	{		
		this.id = id;
		this.departments = departments;
		this.employees = employees;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
	
	public DeptEmpDTO(DeptEmp deptEmp)
	{		
		this.id = new DeptEmpIdDTO(deptEmp.getId());
		this.departments = new DepartmentsDTO(deptEmp.getDepartments());
		this.employees = new EmployeesDTO(deptEmp.getEmployees());
		this.fromDate = deptEmp.getFromDate();
		this.toDate = deptEmp.getToDate();
	}

	public DeptEmpIdDTO getId() {
		return id;
	}

	public void setId(DeptEmpIdDTO id) {
		this.id = id;
	}

	public DepartmentsDTO getDepartments() {
		return departments;
	}

	public void setDepartments(DepartmentsDTO departments) {
		this.departments = departments;
	}

	public EmployeesDTO getEmployees() {
		return employees;
	}

	public void setEmployees(EmployeesDTO employees) {
		this.employees = employees;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
