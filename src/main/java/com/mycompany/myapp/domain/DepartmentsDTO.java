package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.Departments;

public class DepartmentsDTO 
{
	private String deptNo;
	private String deptName;
	
	public DepartmentsDTO()
	{
	}
	
	public DepartmentsDTO(String deptNo, String deptName)
	{
		this.deptNo = deptNo;
		this.deptName = deptName;
	}
	
	public DepartmentsDTO(Departments departments)
	{
		this.deptNo = departments.getDeptNo();
		this.deptName = departments.getDeptName();
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
	
	
	
}