package com.mycompany.myapp.domain;

import com.mycompany.myapp.entity.DeptEmpId;

public class DeptEmpIdDTO {
	
	private int empNo;
    private String deptNo;
    
	public DeptEmpIdDTO()
	{
		super();
	}
	
	public DeptEmpIdDTO(int empNo, String deptNo) 
	{
		this.empNo = empNo;
		this.deptNo = deptNo;
	}
	
	public DeptEmpIdDTO(DeptEmpId deptEmpId) 
	{
		this.empNo = deptEmpId.getEmpNo();
		this.deptNo = deptEmpId.getDeptNo();
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}
	
	public DeptEmpIdDTO toDTO(DeptEmpId deptEmpId)
	{
		return new DeptEmpIdDTO(deptEmpId.getEmpNo(), deptEmpId.getDeptNo());	
	}
}
