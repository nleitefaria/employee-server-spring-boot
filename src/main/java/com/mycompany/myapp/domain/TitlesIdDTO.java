package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.TitlesId;

public class TitlesIdDTO 
{
	private int empNo;
	private String title;
	private Date fromDate;
	
	public TitlesIdDTO()
	{
	}
	
	public TitlesIdDTO(int empNo, String title, Date fromDate)
	{
		this.empNo = empNo;
		this.title = title;
		this.fromDate = fromDate;
	}
	
	public TitlesIdDTO(TitlesId titlesId)
	{
		this.empNo = titlesId.getEmpNo();
		this.title = titlesId.getTitle();
		this.fromDate = titlesId.getFromDate();
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
}
