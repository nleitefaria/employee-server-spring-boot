package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.Employees;

public class EmployeesDTO 
{
	private int empNo;
	private Date birthDate;
	private String firstName;
	private String lastName;
	private String gender;
	private Date hireDate;
	
	public EmployeesDTO() 
	{
	}
	
	public EmployeesDTO(int empNo, Date birthDate, String firstName, String lastName, String gender, Date hireDate) 
	{
		this.empNo = empNo;
		this.birthDate = birthDate;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.hireDate = hireDate;
	}
	
	public EmployeesDTO(Employees employees) 
	{
		this.empNo = employees.getEmpNo();
		this.birthDate = employees.getBirthDate();
		this.firstName = employees.getFirstName();
		this.lastName = employees.getLastName();
		this.gender = employees.getGender();
		this.hireDate = employees.getHireDate();
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
}
