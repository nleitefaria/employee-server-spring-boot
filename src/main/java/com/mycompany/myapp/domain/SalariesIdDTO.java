package com.mycompany.myapp.domain;

import java.util.Date;

import com.mycompany.myapp.entity.SalariesId;

public class SalariesIdDTO 
{
	private int empNo;
    private Date fromDate;
 
	public SalariesIdDTO() 
	{	
	}
	public SalariesIdDTO(int empNo, Date fromDate) 
	{	
		this.empNo = empNo;
		this.fromDate = fromDate;
	}
	
	public SalariesIdDTO(SalariesId salariesId) 
	{	
		this.empNo = salariesId.getEmpNo();
		this.fromDate = salariesId.getFromDate();
	}
	
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
}
