package com.mycompany.myapp.domain;

import java.util.Date;

public class TitlesDTO 
{	
	private TitlesIdDTO id;
    private EmployeesDTO employees;
    private Date toDate;
    
	public TitlesDTO() 
	{	
	}

	public TitlesDTO(TitlesIdDTO id, EmployeesDTO employees, Date toDate) 
	{
		this.id = id;
		this.employees = employees;
		this.toDate = toDate;
	}
	
	public TitlesIdDTO getId() {
		return id;
	}
	public void setId(TitlesIdDTO id) {
		this.id = id;
	}
	public EmployeesDTO getEmployees() {
		return employees;
	}
	public void setEmployees(EmployeesDTO employees) {
		this.employees = employees;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
