package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.TitlesDTO;
import com.mycompany.myapp.service.TitlesService;

@Controller
public class TitlesController
{
	private static final Logger logger = LoggerFactory.getLogger(TitlesController.class);
	
	@Autowired
	TitlesService titlesService;

	@RequestMapping(value = "/titles/{empNo}", method = RequestMethod.GET)
	public @ResponseBody List<TitlesDTO> findByEmpNo(@PathVariable int empNo) 
	{
		logger.info("Listing titles for empNo: " + empNo);
		return titlesService.findTitlesByEmpNo(empNo);
	}
	
	@RequestMapping(value = "/titles", method = RequestMethod.GET)
	public ResponseEntity<List<TitlesDTO>> findAll() 
	{
		logger.info("Listing all titles");
		return new ResponseEntity<List<TitlesDTO>>(titlesService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/titles/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<TitlesDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing department");
		return new ResponseEntity<PagginatedDTO<TitlesDTO>>(titlesService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/titles", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody TitlesDTO titlesDTO)
	{	
		logger.info("Creating title"); 	
		try
		{
			titlesService.save(titlesDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}			
	}
}
