package com.mycompany.myapp.rws;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.EmployeesService;

@Controller
public class EmployeesController 
{	
	private static final Logger logger = LoggerFactory.getLogger(EmployeesController.class);
	
	@Autowired
	EmployeesService employeesService;
	
	@RequestMapping(value = "/employees/{id}", method = RequestMethod.GET)
	public @ResponseBody EmployeesDTO findOne(@PathVariable int id) 
	{
		logger.info("Listing employee with id: " + id);
		return employeesService.findOne(id);
	}

	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public @ResponseBody List<EmployeesDTO> findAll()
	{
		logger.info("Listing all employees");
		return employeesService.findAll();
	}
	
	@RequestMapping(value = "/employees/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<EmployeesDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing employees");
		return new ResponseEntity<PagginatedDTO<EmployeesDTO>>(employeesService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
}
