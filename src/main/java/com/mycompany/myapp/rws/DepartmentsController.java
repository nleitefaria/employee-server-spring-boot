package com.mycompany.myapp.rws;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.DepartmentsService;

@Controller
public class DepartmentsController 
{
	private static final Logger logger = LoggerFactory.getLogger(DepartmentsController.class);
	
	@Autowired
	DepartmentsService departmentsService;
	
	@RequestMapping(value = "/departments/{id}", method = RequestMethod.GET)
	public ResponseEntity<DepartmentsDTO> findOne(@PathVariable String id) 
	{
		logger.info("Listing department with id: " + id);
		return new ResponseEntity<DepartmentsDTO>(departmentsService.findOne(id), HttpStatus.OK);
	}

	@RequestMapping(value = "/departments", method = RequestMethod.GET)
	public ResponseEntity<List<DepartmentsDTO>> findAll()
	{
		logger.info("Listing all departments");
		return new ResponseEntity<List<DepartmentsDTO>>(departmentsService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/departments/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<DepartmentsDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing department");
		return new ResponseEntity<PagginatedDTO<DepartmentsDTO>>(departmentsService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody DepartmentsDTO departmentsDTO)
	{	
		logger.info("Creating department"); 	
		try
		{
			departmentsService.save(departmentsDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
		
	}
	
	@RequestMapping(value = "/departments/{id}", method = RequestMethod.PUT)
	public ResponseEntity<DepartmentsDTO> update(@PathVariable String id, @RequestBody DepartmentsDTO departmentsDTO) 
	{
		logger.info("Updating department with id: " + id);		
		try
		{
			DepartmentsDTO ret = departmentsService.update(id, departmentsDTO);
			if(ret != null)
			{
				logger.info("Done");
				return new ResponseEntity<DepartmentsDTO>(ret, HttpStatus.CREATED);			
			}
			else
			{
				logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<DepartmentsDTO>(HttpStatus.NOT_FOUND);				
			}		
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while updating the actor");
			return new ResponseEntity<DepartmentsDTO>(HttpStatus.BAD_REQUEST);					
		}		
	}
	
	@RequestMapping(value = "/departments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable String id)
	{       
        logger.info("Deleting entity with id: " + id);	      
        try
		{
        	String ret = departmentsService.delete(id);
        	
        	if(ret != null)
        	{
        		logger.info("Done");
        		return new ResponseEntity<String>(id, HttpStatus.NO_CONTENT);       		
        	}
        	else
        	{
        		logger.error("An error ocurred while updating the entity with id: " + id + " , entity does not exists in the db");
				return new ResponseEntity<String>(HttpStatus.NOT_FOUND);       		
        	}      	
		}
        catch(Exception e)
		{
			logger.error("An error ocurred while deleting the entity");
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);					
		}	     
    }
}
