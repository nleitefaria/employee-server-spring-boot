package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mycompany.myapp.domain.SalariesDTO;
import com.mycompany.myapp.service.SalariesService;

@Controller
public class SalariesController
{
	private static final Logger logger = LoggerFactory.getLogger(SalariesController.class);
	
	@Autowired
	SalariesService salariesService;
	
	@RequestMapping(value = "/salaries/{empNo}", method = RequestMethod.GET)
	public @ResponseBody List<SalariesDTO> findByEmpNo(@PathVariable int empNo)
	{
		logger.info("Listing depManager for empNo: " + empNo);
		return salariesService.findDeptManagerByEmpNo(empNo);
	}

	@RequestMapping(value = "/salaries", method = RequestMethod.GET)
	public @ResponseBody List<SalariesDTO> findAll()
	{
		logger.info("Listing all salaries");
		return salariesService.findAll();
	}
	
	@RequestMapping(value = "/salaries", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody SalariesDTO salariesDTO)
	{	
		logger.info("Creating salary"); 	
		try
		{
			salariesService.save(salariesDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
		
	}
}
