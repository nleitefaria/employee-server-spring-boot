package com.mycompany.myapp.rws;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myapp.domain.DeptManagerDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.DeptManagerService;

@RestController
public class DeptManagerController 
{
	private static final Logger logger = LoggerFactory.getLogger(DeptManagerController.class);
	
	@Autowired
	DeptManagerService deptManagerService;
	
	@RequestMapping(value = "/deptManager/{empNo}", method = RequestMethod.GET)
	public @ResponseBody List<DeptManagerDTO> findByEmpNo(@PathVariable int empNo)
	{
		logger.info("Listing depManager for empNo: " + empNo);
		return deptManagerService.findDeptManagerByEmpNo(empNo);
	}
	
	@RequestMapping(value = "/deptManager", method = RequestMethod.GET)
	public @ResponseBody List<DeptManagerDTO> findAll()
	{
		logger.info("Listing all departments");
		return deptManagerService.findAll();
	}
	
	@RequestMapping(value = "/deptManager/paginated", method = RequestMethod.GET)
	public ResponseEntity<PagginatedDTO<DeptManagerDTO>> findAllPaginated(@RequestParam(value="pageNum", defaultValue="1") String pageNum,  @RequestParam(value="pageSize", defaultValue="10") String pageSize) 
	{
		logger.info("Listing department");
		return new ResponseEntity<PagginatedDTO<DeptManagerDTO>>(deptManagerService.findAllPaginated(Integer.parseInt(pageNum), Integer.parseInt(pageSize)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deptManager", method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody DeptManagerDTO deptManagerDTO)
	{	
		logger.info("Creating deptManager"); 	
		try
		{
			deptManagerService.save(deptManagerDTO);
			logger.info("Done");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			logger.error("An error ocurred while creating the entity");
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);					
		}	
		
	}

}
