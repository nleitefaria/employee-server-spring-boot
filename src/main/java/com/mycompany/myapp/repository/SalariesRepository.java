package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.entity.Salaries;
import com.mycompany.myapp.entity.SalariesId;

public interface SalariesRepository extends JpaRepository<Salaries, SalariesId>, JpaSpecificationExecutor<Salaries>
{ 
	@Query("SELECT DISTINCT s FROM Salaries s LEFT JOIN FETCH s.id sid WHERE sid.empNo = (:empNo)")
	List<Salaries> findSalariesByEmpNo(@Param("empNo") int empNo);
}