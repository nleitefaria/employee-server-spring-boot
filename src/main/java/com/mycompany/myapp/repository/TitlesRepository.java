package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.entity.Titles;
import com.mycompany.myapp.entity.TitlesId;

public interface TitlesRepository extends JpaRepository<Titles, TitlesId>, JpaSpecificationExecutor<Titles>
{ 
	@Query("SELECT DISTINCT t FROM Titles t LEFT JOIN FETCH t.id tid WHERE tid.empNo = (:empNo)")
	List<Titles> findTitlesByEmpNo(@Param("empNo") int empNo);
}
