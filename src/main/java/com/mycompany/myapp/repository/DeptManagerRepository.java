package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.mycompany.myapp.entity.DeptManager;
import com.mycompany.myapp.entity.DeptManagerId;

public interface DeptManagerRepository extends JpaRepository<DeptManager, DeptManagerId>, JpaSpecificationExecutor<DeptManager> 
{
	@Query("SELECT DISTINCT dm FROM DeptManager dm LEFT JOIN FETCH dm.id dmid WHERE dmid.empNo = (:empNo)")
	public List<DeptManager> findDeptManagersByEmpNo(@Param("empNo") int empNo);
}
