package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Departments;

public interface DepartmentsRepository extends JpaRepository<Departments, String>, JpaSpecificationExecutor<Departments> {

}
