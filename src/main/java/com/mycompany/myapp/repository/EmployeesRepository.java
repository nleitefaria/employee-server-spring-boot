package com.mycompany.myapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mycompany.myapp.entity.Departments;
import com.mycompany.myapp.entity.Employees;

public interface EmployeesRepository extends JpaRepository<Employees, Integer>, JpaSpecificationExecutor<Departments> { 

}
