package com.mycompany.myapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mycompany.myapp.entity.DeptEmp;
import com.mycompany.myapp.entity.DeptEmpId;


public interface DeptEmpRepository  extends JpaRepository<DeptEmp, DeptEmpId>, JpaSpecificationExecutor<DeptEmp>
{ 
	@Query("SELECT DISTINCT de FROM DeptEmp de LEFT JOIN FETCH de.id deid WHERE deid.empNo = (:empNo)")
	List<DeptEmp> findDeptEmpByEmpNo(@Param("empNo") int empNo);
}
